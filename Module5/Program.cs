﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace princess
{
    public class Game
    {
        int n;
        int nmines;
        int health;
        int x, y;
        string lastMessage;
        bool [,] activatedMines;
        int[,] mines;  

        public static void Main()
        {            
            do
            {
                Console.WriteLine("You have 10 point of health.");
                Console.WriteLine("Let's go to the Princess? Enter - Yes, Esc - No.");
                ConsoleKeyInfo ki = Console.ReadKey(true);
                switch (ki.Key)
                {                    
                    case ConsoleKey.Enter:
                        Game s = new Game(10, 10, 10);
                        if (s.Run())
                        {
                            Console.WriteLine("Game over.You win!");

                        }
                        else
                        {
                            Console.WriteLine("Game over.You lose!");
                        }
                        break;
                    case ConsoleKey.Escape:
                        return;
                }
            } while (true);
        }

        public Game(int n, int nmines, int health)
        {
            Console.ForegroundColor = ConsoleColor.White;
            this.n = n;
            this.x = 0;
            this.y = 0;
            this.nmines = nmines;
            this.health = health;
            this.lastMessage = "";
            mines = new int[n, n];
            activatedMines = new bool[n, n];

            InitField();
        }

        private void InitField()
        {
            Random rnd = new Random(DateTime.Now.Millisecond);
            for (int i = 0; i < nmines; i++)
            {
                int rx;
                int ry;
                do
                {
                    rx = rnd.Next(n);
                    ry = rnd.Next(n);
                }
                while (mines[rx, ry] > 0 || (rx == n-1 && ry == n-1) || (rx == 0 && ry == 0));
                mines[rx, ry] = rnd.Next(n) + 1;
            }           
        }

        private void PrintField(string lastMessage)
        {
            PrintField(false, lastMessage);
        }

        private void PrintField(bool showmines, string lastMessage)
        {
            Console.Clear();
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < n; j++)
                {

                    if ((i == x) && (j == y))
                        Console.ForegroundColor = ConsoleColor.Green;
                    if (showmines)
                        if (mines[i, j] > 0)
                            Console.Write("*");
                        else
                            if (i == n - 1 && j == n - 1)
                                Console.Write("p");
                            else
                                Console.Write("#");
                    else
                        if (activatedMines[i, j])
                            Console.Write("*");
                        else
                            if (i == n-1 && j == n-1)
                                Console.Write("p");
                            else
                                Console.Write("#");
                    if ((i == x) && (j == y))
                        Console.ForegroundColor = ConsoleColor.White;
                }
                Console.WriteLine();
            }
            Console.WriteLine(lastMessage);
        }
       
        public bool Run()
        {
            do
            {
                PrintField(lastMessage);
                if (x == n - 1 && y == n - 1)
                    return true;
                if (health <= 0)
                    return false;

                ConsoleKeyInfo ki = Console.ReadKey(true);
                switch (ki.Key)
                {
                    case ConsoleKey.LeftArrow:
                        y = (y == 0) ? (0) : (y - 1);
                        break;
                    case ConsoleKey.RightArrow:
                        y = (y == n - 1) ? (n - 1) : (y + 1);
                        break;
                    case ConsoleKey.DownArrow:
                        x = (x == n - 1) ? (n - 1) : (x + 1);
                        break;
                    case ConsoleKey.UpArrow:
                        x = (x == 0) ? (0) : (x - 1);
                        break;
                }

                if (mines[x, y] > 0)
                {
                    health = (mines[x, y] > health) ? (0) : (health - mines[x, y]);
                    
                    lastMessage = "Boom! You lost " + mines[x, y] +  " points of health.\n"
                        + "You have " + health + " points of health.";
                    
                    mines[x, y] = 0;
                    activatedMines[x, y] = true;
                }                            
            }
            while (true);
        }
    }
}